/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Kent
 *
 * Created on September 13, 2017, 3:53 PM
 */

#include <iostream>        
#include <cstdlib>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    const int  SIZE = 8;
    int set[SIZE] = {5, 10, 15, 20, 25, 30, 35, 40};
    int *numPtr = NULL;      // Pointer
    int count;                  // Counter variable for loops
    
    // Make numPtr point to the set array
    numPtr = set;
    
    // Use the pointer to display the array contents
    cout << "The numbers in set are:\n";
    for (count = 0; count < SIZE; count++)
    {
        cout << *numPtr << " ";
        numPtr++;
    }
    
    // Display the array contents in reverse order
    cout << "\nThe numbers in set backward are:\n";
    for (count = 0; count < SIZE; count++)
    {
        numPtr--;
        cout << *numPtr << " ";
    }
    return 0;
}

