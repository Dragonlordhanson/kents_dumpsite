/*
 * Diagnosis of Diabetes is a program for a person. Who is wondering if they may or 
 * may not have diabetes, but is not patient enough to wait for the doctors lab results.
 * My father was a diabetic, so there is a higher risk of one of his children 
 * getting the diabetes, This program is something dear to me.
 */

/*
 * Name: Kent Hanson
 * Student ID: 1168787
 * Date:  October 1, 2017, 1:53 PM
 * HW:
 * Problem:
 * I certify this is my own work and code
 */

#include <iostream>        
#include <cstdlib>
#include <cctype>

#include <cstring>
#include "ClearScreen.h"       // clears the screen
#include "DiagnosisDiabetes.h" // include Diagnosis Diabetes Systom class

using namespace std;
DiagnosisDiabetes dds;

char ch;    // Define global variable

/*
 * 
 */
int main(int argc, char** argv) {
    
    int choice;
    
    dds.welcome_screen();
    
    switch(choice)
    {
        case 1:
            dds.personalInformation;
            break;
        case 2:            
            break;
        case 3:
            break;
        default:
            cout << "Invalid choice";
    }

    return 0;
}

void DiagnosisDiabetes::welcome_screen() {

    clrscr();
    cout << "\t\tWelcome to self Diabetes Diagnosis System." << endl;
    cout << "\t\t\tDone by: Kent Hanson." << endl;
    cout << "\n\n";
    cout << "Please press a key to get started." << endl;
    cin.get(ch);
   // return 0;
}

DiagnosisDiabetes::personalInformation(char patientsName, int patientsAge, double patientsWeight,
       double patientsHeght, char patientsGender)
 {
    clrscr(); 
    cout << "\tPersonal Information" << endl;
    cout << "\nThe name of the patient: " << endl;
    std::cin.getline(dds.patientsName, SIZE);
    cout << "The age of the patient: " << endl;
    cin >> dds.patientsAge;
    cout << "The weight of the patient: " << endl;
    cin >> dds.patientsWeight;
    cout << "The height of the patient: " << endl;
    cin >> dds.patientsHeght;
    cout << "What is the patient's gender: " << endl;
    cin >> dds.patientsGender;
    
}

void diagnosticScreen(void)
{
	clrscr();
	cout << "\t\tDiagnostic Screen" << endl;
	cout << endl << endl;
	cout << "Press enter the key to see the results";
        cin.get();	
}

void diagnosisSymptoms_one(void)
{
    clrscr();
    cout<< "\t\tMEDICAL DIAGONOSIS FORM" <<endl <<endl;
    for (int i = 0; i < 9; i++)
    {
        cout<<"FREQUENCY OF THIRST(H(HIGH), L(LOW), N(NORMAL):" << endl; 
        cin >> dds.sq;
        cout<<"FREQUENCY OF URINATION(H(HIGH), L(LOW), N(NORMAL):" << endl;
        cin >> dds.sq;
        cout<<"VISION (I(IMPAIRMENT), N(NORMAL)" << endl;
        cin >> dds.sq;
        cout<<"URINE SUGAR(P(PASSIVE) A(ACTIVE);" << endl;
        cin >> dds.sq;
        cout<<"KETONUREA(P(PASSIVE) A(ACTIVE)" << endl;
        cin >> dds.sq;
        cout<<"FASTING BLOOD SUGAR(H(HIGH) L(LOW) N(NOMAL)" << endl;
        cin >> dds.sq;
        cout<<"Random Blood Sugar (H(HIGH) L(LOW) N(NORMAL)" << endl;
        cin >> dds.sq;
        cout<<"Family History of Diabetes - P (PASSIVE) A(ACTIVE)" << endl;
        cin >> dds.sq;
        cout<<"Oral Glucose Tolerance Test (D/N)" << endl;
        cin >> dds.sq;
    }
}

void diagnosisSymptoms_two(void)
{
    
}

void diagnosisSymptoms_three(void)
{
    
}

int diagnosticSymptoms(int)
{
    
}

char displayWindow(int,int)
{
    
}