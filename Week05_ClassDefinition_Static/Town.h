/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Town.h
 * Author: Kent
 *
 * Created on September 29, 2017, 7:43 AM
 */

#ifndef TOWN_H
#define TOWN_H

class Town {
public:
    Town();
    Town(const Town& orig);
    virtual ~Town();
    int curSize;
    void outputCurrentCount();
    static void increaseMaxSize(int);
    
private:
    static int townCount;
    static int maxSize;

};

#endif /* TOWN_H */

