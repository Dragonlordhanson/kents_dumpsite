/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Kent
 *
 * Created on September 15, 2017, 6:39 AM
 */

#include <iostream>        
#include <cstdlib>
#include <cctype>

using namespace std;

// Structures
struct C_A // C_A = Customer Accounts
{
    char firstName[40];            // Customer's first name
    char lastName[40];             // Customer's last name
    char mailingAddress[30];       // Customer's mailing address
    char residentialAddress[30];   // Customer's residential address
    string city;                   // Customer's residential location   
    string state;                  // Customer's home state
    int    zipCode;                // Customer's zip code   
    int    phoneNumber;            // Customer's phone Number
    
};

struct creditCard
{
    double accountBalance;       // Customer's Account Balance   
    int    lastPayment;          // Customer's Last Payment   
};

/*
 * 
 */
int main(int argc, char** argv) {
    
    int choice;
    // The main menu function display below here
    cout << "*********** Main Menu ****************\n\n";
    cout << "1. Create a new account.\n";
    cout << "2. Display a Customer's Account.\n";
    cout << "3. Delete a Customer's Account.\n";
    cout << "4. Change Customer's Account Information.\n";
    cout << "5. Quit the Program.\n";
    
        
    // Switch statements
    if (choice < 1 || choice > 5)
      cout << "\nPlease enter a number in the range 1 - 5";
    else
    {
      switch(choice)
      {
        case 1: cout << "You have selected item 1.";
                break;
        case 2: cout << "You have selected item 2.";
                break;
        case 3: cout << "You have selected item 3.";
                break;
        case 4: cout << "You have selected item 4.";
                break;
        case 5: cout << "You have selected item 5.";
                break;
      }
    }
    
    
    // Creating Customer Accounts information
    C_A clientInfo;
    
    cout << "Enter the Customer's first: ";
    cin >> clientInfo.firstName;
    cout << "Enter customer's last name: ";
    cin >> clientInfo.lastName;
    cout << "Enter customer's mailing address:\n";
    cin >> clientInfo.mailingAddress;
    cout << "Enter customer's residential address:\n";
    cin >> clientInfo.residentialAddress;
    cout << "Enter customer's home city: ";
    cin >> clientInfo.city;
    cout << "Enter customer's state: ";
    cin >> clientInfo.state;
    cout << "Enter customer's zip code: ";
    cin >> clientInfo.zipCode;
    cout << "Enter customer's day time phone number: ";
    cin >> clientInfo.phoneNumber;
    cin.ignore();
    
            
    return 0;
}

