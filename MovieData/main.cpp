/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Name: Kent Hanson
 * Student ID: 1168787
 * Date:  September 20, 2017, 9:47 AM
 * HW: Movie Data
 * Problem:
 * I certify this is my own work and code
 */

#include <iostream>        
#include <cstdlib>

using namespace std;

// Constant section
const int SIZE = 100; // Array size

// Structure function
struct movieData
{
    char title[SIZE];         // Title of the movie
    char director[SIZE];      // Director of the movie
    int releaseYear; // Year it was released
    int movieLength;    // How many minutes is the movie
};
/*
 * 
 */

int main(int argc, char** argv) 
{
    movieData motionPictureOne = {"The Hobbit", "Peter Jackson", 2012, 169};
    movieData motionPictureTwo = {"JAWS", "Steven Spielberg", 1975, 124};
    movieData motionPictureThree = {"Harry Potter and the Sorcerer's Stone", "Chris Columbus", 2001, 152};
    
    
    cout << "The first movie in the database." << endl;
    cout << "\n";
    cout << "Title of the movie: " << motionPictureOne.title << endl;
    cout << "Who directed the movie: " << motionPictureOne.director << endl;
    cout << "The year the movie was released: " << motionPictureOne.releaseYear << endl;
    cout << "How many minutes the movie is running: " << motionPictureOne.movieLength << endl;
    cout << "\n";
    cout << "The second movie in the database." << endl;
    cout << "\n";
    cout << "Title of the movie: " << motionPictureTwo.title << endl;
    cout << "Who directed the movie: " << motionPictureTwo.director << endl;
    cout << "The year the movie was released: " << motionPictureTwo.releaseYear << endl;
    cout << "How many minutes the movie is running: " << motionPictureTwo.movieLength << endl;
    cout << "\n";
    cout << "The third movie in the database." << endl;
    cout << "\n";
    cout << "Title of the movie: " << motionPictureThree.title << endl;
    cout << "Who directed the movie: " << motionPictureThree.director << endl;
    cout << "The year the movie was released: " << motionPictureThree.releaseYear << endl;
    cout << "How many minutes the movie is running: " << motionPictureThree.movieLength << endl;
    return 0;
}

