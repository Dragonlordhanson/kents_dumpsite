/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Title: Create a function that creates an array
 * File:   main.cpp
 * Author: Kent Hanson
 * Student ID: 1168787
 * Created on September 1, 2017, 10:32 AM
 */

#include <iostream>
#include <cstdlib>
#include <iomanip>

using namespace std;


// add a value to dynamic array

void addValue(int* p, int size, int value)
{
    // Create new array
    int * p2 = new int[ size + 1];
    for (int i = 0; i < size; i++) // Copies values from original to new
    {
        p2[i] = p[i];
    }
    p2[size] = value; // Stores value at int of new array
    delete [] p;
    p = p2;
    size++; // change size
}   
/*
 *
 */

int main(int argc, char** argv) {
    int size = 0;
    int* pArray = new int[size];
    addValue(pArray, size, 1);
    addValue(pArray, size, 10);
    for (int i = 0;i < 10; i++)
        addValue(pArray, size, i);
    
}

void delValue(int* &p, int &size, int &value )  
{
    if (size == 0) return;
    // Create new array
    int * p2 = new int[ size - 1];
    for (int i = 0; i < size - 1; i++) // Copies values from original to new
    {
        p2[i] = p[i];
    }
    p2[size] = value; // Stores value at int of new array
    delete [] p;
    p = p2;
    size--; // change size
}   