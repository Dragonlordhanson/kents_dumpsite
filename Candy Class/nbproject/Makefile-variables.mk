#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=MinGW_TDM_1-Windows
CND_ARTIFACT_DIR_Debug=dist/Debug/MinGW_TDM_1-Windows
CND_ARTIFACT_NAME_Debug=candy_class
CND_ARTIFACT_PATH_Debug=dist/Debug/MinGW_TDM_1-Windows/candy_class
CND_PACKAGE_DIR_Debug=dist/Debug/MinGW_TDM_1-Windows/package
CND_PACKAGE_NAME_Debug=candyclass.tar
CND_PACKAGE_PATH_Debug=dist/Debug/MinGW_TDM_1-Windows/package/candyclass.tar
# Release configuration
CND_PLATFORM_Release=MinGW_TDM_1-Windows
CND_ARTIFACT_DIR_Release=dist/Release/MinGW_TDM_1-Windows
CND_ARTIFACT_NAME_Release=candy_class
CND_ARTIFACT_PATH_Release=dist/Release/MinGW_TDM_1-Windows/candy_class
CND_PACKAGE_DIR_Release=dist/Release/MinGW_TDM_1-Windows/package
CND_PACKAGE_NAME_Release=candyclass.tar
CND_PACKAGE_PATH_Release=dist/Release/MinGW_TDM_1-Windows/package/candyclass.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
