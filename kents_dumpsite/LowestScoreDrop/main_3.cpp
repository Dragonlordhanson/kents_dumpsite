/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main_3.cpp
 * Author: Kent
 *
 * Created on September 12, 2017, 3:43 PM
 */

#include <iostream>    '
#include <iomanip>    
#include <cstdlib>

using namespace std;

// Function Prototype:
void getScore(int, int, int, int, int, int);
void calcAverage(int, int, int, int, int, int);
int findLowest(int, int);

/*
 * 
 */
int main(int argc, char** argv) {
    
    int score1, score2, score3;  // To hold three test scores
    double average;              // To hold the average score
    
    
    return 0;
}

void getScore(int, int, int, int, int, int)
{
    // Get the three test score.
    cout << "Enter 3 test scores, and I will average them: ";
    cin >> score1 >> score2 >> score3;
}

void calcAverage(int, int, int, int, int, int)
{
    // Calculate and display the averagescore.
    average = (score1 + score2 + score3) / 3.0;
    cout << fixed << showpoint << setprecision(1);
    cout << "Your average is " << average << endl;
}

int findLowest(int, int)
{
    const int HIGH_SCORE  = 95;  // A high score is 95 or greater
    
    // If the average is a high score, congratulate the user.
    if (average > HIGH_SCORE)
        cout << "Congratulations! That's a high score!\n";
}