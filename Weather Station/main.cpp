/*
 * This program collects the weather statistics for a year. 
 * The data it collects is total rainfall, high temp, low temp, average temp.
 */

/*
 * Name: Kent Hanson
 * Student ID: 1168787
 * Date:  September 26, 2017, 11:02 AM
 * HW:
 * Problem:
 * I certify this is my own work and code
 */


#include <iostream>        
#include <cstdlib>
#include <iomanip>
#include <string>

using namespace std;

enum Months { JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE, 
       JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER };
// Constants
const int SIZE = 5; // setting the size 

// Structure
struct weatherStatistics
{
    float TotalRainfall;
    float HighTemperature;
    float LowTemperature; 
    float AverageTemperature;
    
};

// Prototypes


// Main part of the program
int main(int argc, char** argv) {
    
    const int TWELVEMONTHS = 12; // Number of months
    weatherStatistics *weatherStation = new weatherStatistics[TWELVEMONTHS]; // Creating Array structures
    int index;  // Initializing loop
    float yearlyTotal;
    
    // Gathering up some of the players information
    for (index = JANUARY; index <= DECEMBER; index++)
    {
        cout << "Please enter the Amount of rain that fell: ";
        cin >> weatherStation[index].TotalRainfall;
        
        // Validating user input to makes sure it is positive number
        while (weatherStation[index].TotalRainfall < 0)
        {
            cout << "Please enter a positive number." << endl;
            cin >> weatherStation[index].TotalRainfall;
        }
        
        
        cout << "Please enter the high temperature: ";
        cin >> weatherStation[index].HighTemperature;
        cout << "Please enter the low temperature: ";
        cin >> weatherStation[index].LowTemperature;
        
    }                                   
    //Display the players data
    cout << "Here is the data on the yearly rainfall: " << endl << endl;
    cout << "\tMonth\t\tRainFall\tHi/Low Temp. " << endl;
    cout << "--------------------------------" << endl;
    for (index = JANUARY; index <= DECEMBER; index++)
    {
            cout << weatherStation[index].TotalRainfall
                 << weatherStation[index].HighTemperature
                 << weatherStation[index].LowTemperature <<endl;
    }
    //Calculate the total points
    for (index = JANUARY; index <= DECEMBER; index++)
    {
            yearlyTotal += weatherStation[index].TotalRainfall;

    }
    
    //Display the results of the total points.
    cout << "\n\nThe average rainfall for the year is: ";
    cout << yearlyTotal << endl;
    
    // Delete the memory.
    delete [] weatherStation;
    return 0;
}

