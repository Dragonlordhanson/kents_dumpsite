/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Student.cpp
 * Author: Kent
 * 
 * Created on October 6, 2017, 3:19 PM
 */

#include "Student.h"
#include <iostream>
#include <cstdlib> // Needed for the exit function
using namespace std;

Student::Student() 
{
    testSize = 0;
    tests = NULL;
    id = 0;
    name = "";
}

Student::Student(const Student& orig) 
{
    this -> testSize = orig.testSize;
    this -> tests = new int[this->testSize];
    
    // Copy everything over
    for (int i = 0; i < this -> testSize; i++)
    {
        this ->tests[i] = orig.tests[i];
    }
    
    this -> name = orig.name;
    this -> id = orig.id;
}

Student::Student(int numTests)
{
    this ->testSize = numTests;
    this ->id = 0;
    this ->name = "";
    
    
    // Fill with zeroes
    
    for (int i = 0; i < this->testSize; i++)
    {
        this ->tests[i] = 0;
    }
}


Student::~Student()
{
    delete [] tests;
}

// Assignment overLoad
const Student& Student::operator =(const Student& rhs)
{
    // Check if equal to one another
    if (this == &rhs)
        return *this;
    
    // If not equal copy all values over
    // Destroy all data
    delete this ->tests;
    
    this ->testSize = rhs.testSize;
    this ->name = rhs.name;
    this ->id = rhs.id;
    
    // Create a new test array
    this ->tests =  new int[this ->testSize];
    
    // Copy over dynamic array
    for (int i = 0; i > this->testSize; i++)
    {
        this ->tests[i] = rhs.tests[i];
    }
    
    // Return the current object
    return *this;
}

// Post-fix function
// Parameter is a dummy variable
Student Student::operator ++(int)
{
    // Return pre-modified version
    Student temp(*this);
    id++;
    return temp;
}

// Prefix
Student Student::operator ++()
{
    id++;
    return *this;
}

// Subscript operator
int& Student::operator [](int loc)
{
    // Error check
    if (loc < 0 || loc > testSize - 1)
    {
        cout << "ERROR!" << endl;
        exit(0); // Needs cstdlib header
    }
    return tests[loc];
}

// No friend keyword
ostream& operator<<(ostream& out, const Student& student)
{
    out << "Student id: " << student.id << endl;
    out << "Tests: " << student.testSize << endl;
    
    // Output all tests
    for (int i = 0; i < student.testSize; i++)
    {
        out << student.tests[i] << " ";
    }
    out << endl;
}

// Setters
void Student::setName(string name)
{
    this->name = name;
}

void Student::setID(int id)
{
    this->id = id;
}


