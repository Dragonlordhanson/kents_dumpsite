/*
 * This class organizes what should be asked and stored of the person/user 
 */

/* 
 * File:   DiagnosisDiabetes.h
 * Author: Kent
 *
 * Created on October 1, 2017, 1:54 PM
 */

#ifndef DIAGNOSISDIABETES_H
#define DIAGNOSISDIABETES_H
const int SIZE = 100;

class DiagnosisDiabetes {
public:
    char patientsName [SIZE];          // Patient's Name
    int patientsAge;                   // Patient's Age
    double patientsWeight;             // Patient's Weight
    double patientsHeght;              // Patient's height
    char patientsGender;               // Patient's Sex
    char *sq;                          // Patient's Survey Question 
    
    // Diagnosis Diabetes System Windows
    void welcome_screen(void);
    int personalInformation(char, int, double, double, char);
    void diagnosisWindow(void);
    void diagnosisSymptom(void);
    DiagnosisDiabetes();
    DiagnosisDiabetes(const DiagnosisDiabetes& orig);
    virtual ~DiagnosisDiabetes();
    
    // Getter/Setter
    int getfreqUrination();
    int getincrHunger();
    int getweightLoss();
    int gettiredNess();
    int getintersetContration();
    int gethandsFeet();
    
private:
    int freqUrination;
    int incrHunger;
    int weightLoss;
    int tiredNess;
    int intersetContration;
    int handsFeet;

};

#endif /* DIAGNOSISDIABETES_H */

