/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Kent
 *
 * Created on September 9, 2017, 8:42 AM
 */

#include <iostream>   
#include <iomanip>
#include <cstdlib>

using namespace std;


// Function prototype
int getScore(int, int, int &totalScore);
int findLowest(int testScoreArray[5]);
float calcAverage(int, int);




/*
 * 
 */
int main()
{
    int testScoreArray[5];
    int testScore = 0;
    float avg = 0.0f;
    int lowest = 0;
    int totalScore = 0;
    
    
    // Populating 
    
    for(int examNumber = 1; examNumber <= 5; examNumber++)
    {
        testScore = getScore(testScore, examNumber, totalScore);
        testScoreArray[examNumber-1];
    }
     
    lowest = findLowest(testScoreArray);
    avg = calcAverage(lowest, totalScore);
    
    cout << "\nThe lowest score dropped was " << lowest << endl;
    cout << "The average is " << setprecision(1) << avg;
     
    return 0;    
    
            
}

int getScore(int testScore, int examNumber, int &totalScore)
{
     // Gathering exam data:
    cout << "Please enter test score " << examNumber << ": ";
    cin >> testScore;

    // validating user's input
    
    while (testScore < 1 || testScore > 100)
    {
        cout << "ERROR: Please enter Test score values 0 to 100! ";
        cin >> testScore;
    }
    totalScore += testScore;
    return testScore;
}

int findLowest(int testScoreArray[5])
{
    int smallestExamScore = testScoreArray[0];
    for (int examNumber = 1; examNumber < 5; examNumber++)
    {
        if (testScoreArray[examNumber] < smallestExamScore)
            smallestExamScore = testScoreArray[examNumber];
    }
    return smallestExamScore;
}

float calcAverage(int lowest, int totalScore)
{
    int totalSum = 0;
    float avg = 0.0f;
    
    totalSum = totalScore - lowest;
    avg = totalSum / 4.0;
    return avg;
}