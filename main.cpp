/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Kent Hanson
 *
 * Created on August 31, 2017, 8:14 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

void output(int * p, int size);
/*
 * 
 */
int main(int argc, char** argv) {

    // Allocating a memory
    srand(time(0));
    int* p = new int;
    
    // derefence p to store a value
    *p = 4;
    
    cout << "p: " << p << endl;
    cout << "*p: " << *p << endl;
    cout << "address of p: " << &p << endl;
    
    // Create a dynamic array
    int size = 4;
    int* pArray = new int[size];
    
    // store random values in dynamic array
    for (int i = 0; i < size; i++)
    {
        pArray[i] = rand() % 100; // 0-99
    }
    
    // Out dynamic array to the console
    // deallocate data
    delete p;
    delete[] pArray;
    
    return 0;
}


/// Output an array of the console
/// \param p - Array

void output(int * p, int size)
{
    for (int i = 0; i < size; i++)
        cout << p[i] << " ";
    cout << endl;
}

