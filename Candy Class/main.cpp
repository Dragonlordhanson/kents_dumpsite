/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Name: Kent Hanson
 * Student ID: 1168787
 * Date:  September 22, 2017, 9:17 AM
 * HW:
 * Problem:
 * I certify this is my own work and code
 */

#include <iostream>
#include <cstdlib>
#include <string>


using namespace std;

enum CandyType {SOUR, CRUNCHY, SPICY, SWEET};

class Candy
{
    private:
        int calories;
    public:
        string flavor;
        string name;
        CandyType type;

        // default constructor
        Candy();
        Candy (int, string, string, CandyType);

        // Getter/Setter
        int getCalories();
        void setCalories(int);

        // Output function
        void output(); // Output candy
};

// Function prototype
string candyType(CandyType);


/*
 *
 */
int main(int argc, char** argv) {

  // Create a default candy
  Candy snicker;
  snicker.output();

  Candy twix(300, "Chocolate", "Twix", CRUNCHY);
  twix.output();
  return 0;
}

// Class function definition
//Default constructor

Candy::Candy()
{
    calories = 0;
    flavor = "bland";
    name = "";
    type = SOUR;
}

Candy::Candy(int calories, string flavor,
        string name, CandyType type)
{
    this -> calories = calories;
    this -> flavor = flavor;
    this -> name = name;
    this -> type = type;
}

int Candy::getCalories()
{
    return calories;
}

void Candy::setCalories(int calories)
{
    this -> calories = calories;
}

// Output a candy
void Candy::output()
{
    cout << "\n";
    cout << "Number of calories: " << calories << endl
            << "Name: " << name << endl
            << "Flavor: " << flavor << endl
            << "Type of candy: " << candyType(type) << endl;
}

string candyType(CandyType type)
{
  // Output the type of candy
  switch (type)
  {
    case SOUR: return "sour";
    case CRUNCHY: return "crunchy";
    case SWEET: return "sweet";
    case SPICY: return "Spicy";
    default: return "No type";

  }
}
