/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Name: Kent Hanson
 * Student ID: 1168787
 * Date:  October 6, 2017, 3:19 PM
 * HW:
 * Problem:
 * I certify this is my own work and code
 */

 
#include <cstdlib>
#include "Student.h"
#include <iostream>       
#include <fstream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    Student dave(5);
        dave.setName("Dave");
    
    cout << dave;
    
    dave.setID(1000);
    dave++;
    
    cout << dave;
    
    // Change Dave's tests
    dave[0] = 20;
    dave[1] = 100;
    dave[2] = 135;
    
    cout << dave;
    
    // Write dave to a file
    ofstream fout;
    fout.open("Student.txt", ios::app); // Append to file
            
    fout << dave;
    
    fout.close();
    
    return 0;
}

