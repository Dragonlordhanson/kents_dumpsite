/*
 Week 4, First project 11.6 - Soccer score 
 */

/*
 * Name: Kent Hanson
 * Student ID: 1168787
 * Date:  September 25, 2017, 12:37 PM
 * HW:
 * Problem:
 * I certify this is my own work and code
 */

#include <iostream>        
#include <cstdlib>
#include <iomanip>
#include <string>

using namespace std;

// Constants
const int SIZE = 100; // setting the size 


// Structure
struct SoccerScores
{
    char PlayersName[SIZE];   // Player's name
    int PlayersNumber;  // The player's number
    int PointsScored;   // Points scored by players
};

// Prototypes
int platerTotalPoints(int*, int);

// Main part of the program
int main(int argc, char** argv) {
    
    const int NUMBER_OF_PLAYERS = 3; // Number of players
    SoccerScores *PlayersScore = new SoccerScores[NUMBER_OF_PLAYERS]; // Creating Array structures
    int index;  // Initializing loop
    int TotalScore = 0;
    
    // Gathering up some of the players information
    for (index = 0; index < NUMBER_OF_PLAYERS; index++)
    {
        cout << "\n";
        cout << "Please enter the name of the player: ";
        cin.getline(PlayersScore[index].PlayersName, 100);
        cout << "Please enter the number of the player: ";
        cin >> PlayersScore[index].PlayersNumber;

        while (!cin || (PlayersScore[index].PlayersNumber < 0))  //the number is negative
        {
            cout << "ERROR! You have entered a zero or negative number this is not allowed\n";
            cout << "Please enter a positive score: ";
            cin >> PlayersScore[index].PlayersNumber;

        }
        
        cout << "Please enter points scored: ";
        cin >> PlayersScore[index].PointsScored;
        
        while (!cin || (PlayersScore[index].PointsScored < 0))  //the number is negative
        {
            cout << "ERROR! You have entered a zero or negative number this is not allowed\n";
            cout << "Please enter a positive score: ";
            cin >> PlayersScore[index].PointsScored;
            
        }
        cin.ignore();
    }
    //Display the players data
    cout << "Here is the players data: " << endl << endl;
    cout << "    Name    Number    Score" << endl;
    cout << "--------------------------------" << endl;
    for (index = 0; index < NUMBER_OF_PLAYERS; index++)
    {
            cout << setw(8) << PlayersScore[index].PlayersName <<
                    setw(8) << PlayersScore[index].PlayersNumber <<
                    setw(8) << PlayersScore[index].PointsScored << endl;
    }
    //Calculate the total points
    for (index = 0; index < NUMBER_OF_PLAYERS; index++)
    {
            TotalScore += PlayersScore[index].PointsScored;

    }
    
    //Display the results of the total points.
    cout << "\n\nThe total of points scored by the team are: ";
    cout << TotalScore << endl;
    
    // Delete the memory.
    delete [] PlayersScore;
    return 0;
}

