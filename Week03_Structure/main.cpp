/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Name: Kent Hanson
 * Student ID: 1168787
 * Date:  September 22, 2017, 6:54 PM
 * HW:
 * Problem:
 * I certify this is my own work and code
 */

#include <iostream>        
#include <cstdlib>
#include <cctype>

using namespace std;

// Struct definition
struct Classroom
{
    string location;
    int numStudent;
    
};

// Prototype function
void output(const Classroom&);

/*
 * 
 */
int main(int argc, char** argv) {
    
    // Create an array of classrooms
    int SIZE = 20;
    Classroom building[SIZE];
    
    // Create a single classroom
    Classroom be208 = { "BE", 36 };
    
    // Not all members need to be assigned
    Classroom be200 = { "Upstair" };
    
    building[0] = be208;
    building[1] = be200;
    
    // Output all buildings to the screen
    for (int i = 0; i < SIZE; i++)
    {
        output(building[i]);
    }
    
    
    return 0;
}

// Pass ADTs by reference
// make the variable constant to avoid changing data
void output(const Classroom &classRoom)
{
    cout << "Location: " << classRoom.location << endl;
    cout << "Number of strudents: " << classRoom.numStudent << endl << endl;
    
}