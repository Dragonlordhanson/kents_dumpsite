/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * 9.13 Movie Statistics
 * File:   main.cpp
 * Author: Kent
 * 
 * Created on September 13, 2017, 7:19 PM
 */

#include<iostream>
#include <iomanip>

using namespace std;

int main()
{
	int *studentnum;
	int studentsSurveyed;
	int count;
	int total;
	double averageMoviesWatched;
	double median;

    cout << "How many student are being surveyed? " << endl;
    cin >> studentsSurveyed;

    studentnum = new int [studentsSurveyed];

    cout << "Write by order, the number of movies watched by each of the students" << endl;
    for (count = 0; count < studentsSurveyed ; count++)
    {
            cout << "Student#"<< count +1 << ":" << endl;
            cin >> studentnum[count];
            total += studentnum[count];

    }

    cout << "The total of movies watched by all the students is:" << total << " movie." <<  endl;
    
    // Calculating the average movies that have seen 
    averageMoviesWatched = total/studentsSurveyed;
    
    cout << "The Average of movies watched is:" << averageMoviesWatched << " movie." << endl;
   
    median = total / 2;

    
    cout << "The median of movies watched is:" << median << " movies." << endl;
    

    delete [] studentnum;
    studentnum = 0;

    return 0;
}