/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Town.cpp
 * Author: Kent
 * 
 * Created on September 29, 2017, 7:43 AM
 */

#include "Town.h"
#include <iostream>

Town::Town() 
{
    curSize = 0;
    townCount++;
}

Town::Town(const Town& orig)
{
    this -> curSize = orig.curSize;
    townCount++;
}

Town::~Town() 
{
    townCount--;
}

void Town::increaseMaxSize(int size)
{
    maxSize = size;
}
    
void Town::outputCurrentCount()
{
    std::cout << "The current size is: " << townCount << std::endl;
}

int Town::townCount = 0;
int Town::maxSize = 60;

