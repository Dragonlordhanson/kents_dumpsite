int linearSearch(int *p, int val, int size)
{
 // iterate through all values
 for (int i = 0; i < size; i++)	
 {
 	if (val == p[i])
 		return i;
 }	
 return -1;
}
