/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Student.h
 * Author: Kent
 *
 * Created on October 6, 2017, 3:19 PM
 */

#ifndef STUDENT_H
#define STUDENT_H
#include <string>
using namespace std;

class Student {
private:
    int testSize;
    // Array of tests
    int* tests;
    string name;
    int id;
    
public:
    Student();
    Student(const Student& orig); // Copy
    Student(int); // Create initial tests
    // Assignment overload
    const Student& operator=(const Student& rhs);
    virtual ~Student();

    // Set the name and ID
    void setName(string name);
    void setID(int id);
    
    // Post-fix
    Student operator++(int);
    
    // Prefix
    Student operator++();
    
    // Subscript
    int& operator[](int loc);
    
    // Friend function
    friend ostream& operator<<(ostream&, const Student&);
    friend istream& operator>>(istream&, const Student&);
    
};

#endif /* STUDENT_H */

