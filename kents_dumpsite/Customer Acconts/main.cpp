/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Kent
 *
 * Created on September 15, 2017, 6:39 AM
 */

#include <iostream>        
#include <cstdlib>
#include <cctype>

using namespace std;

// Structures
struct C_A // C_A = Customer Accounts
{
    char firstName[40];          // Customer's first name
    char lastName[40];           // Customer's last name
    string mailingAddress;       // Customer's mailing address
    string residentialAddress;   // Customer's residential address
    string city;                 // Customer's residential location   
    string state;                // Customer's home state
    int    zipCode;              // Customer's zip code   
    int    phoneNumber;          // Customer's phone Number
    
};

struct creditCard
{
    int    accountBalance;       // Customer's Account Balance   
    int    lastPayment;          // Customer's Last Payment   
};

/*
 * 
 */
int main(int argc, char** argv) {
    
    // Creating Customer Accounts information
    C_A clientInfo;
    
    cout << "Enter the Customer's first: ";
    getline(cin, clientInfo.firstName);
    cout << "Enter customer's last name: ";
    getline(cin, clientInfo.lastName);
    cout << "Enter customer's mailing address:\n";
    getline(cin, clientInfo.mailingAddress);
    cout << "Enter customer's residential address:\n";
    getline(cin, clientInfo.residentialAddress);
    cout << "Enter customer's home city: ";
    getline(cin, clientInfo.city);
    cout << "Enter customer's state: ";
    getline(cin, clientInfo.state);
    cout << "Enter customer's zip code: ";
    getline(cin, clientInfo.zipCode);
    cout << "Enter customer's day time phone number: ";
    getline(cin, clientInfo.phoneNumber);
    cin.ignore();
    
            
    return 0;
}

