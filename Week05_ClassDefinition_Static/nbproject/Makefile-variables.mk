#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=MinGW_TDM_1-Windows
CND_ARTIFACT_DIR_Debug=dist/Debug/MinGW_TDM_1-Windows
CND_ARTIFACT_NAME_Debug=week05_classdefinition_static
CND_ARTIFACT_PATH_Debug=dist/Debug/MinGW_TDM_1-Windows/week05_classdefinition_static
CND_PACKAGE_DIR_Debug=dist/Debug/MinGW_TDM_1-Windows/package
CND_PACKAGE_NAME_Debug=week05classdefinitionstatic.tar
CND_PACKAGE_PATH_Debug=dist/Debug/MinGW_TDM_1-Windows/package/week05classdefinitionstatic.tar
# Release configuration
CND_PLATFORM_Release=MinGW_TDM_1-Windows
CND_ARTIFACT_DIR_Release=dist/Release/MinGW_TDM_1-Windows
CND_ARTIFACT_NAME_Release=week05_classdefinition_static
CND_ARTIFACT_PATH_Release=dist/Release/MinGW_TDM_1-Windows/week05_classdefinition_static
CND_PACKAGE_DIR_Release=dist/Release/MinGW_TDM_1-Windows/package
CND_PACKAGE_NAME_Release=week05classdefinitionstatic.tar
CND_PACKAGE_PATH_Release=dist/Release/MinGW_TDM_1-Windows/package/week05classdefinitionstatic.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
