/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Kent
 *
 * Created on September 5, 2017, 1:31 PM
 * Still waiting on my book from amazon. 
 */

// The header library section
#include <iostream>        
#include <cstdlib>
#include <ctime>
//----------------------------

using namespace std;

int coinToss(void);

/*
 * 
 */

int main(int argc, char** argv) {

    int num = 0;
    int randomNumber = 0;
    string flipflop = "";
        
    cout << "Enter how many times you want to flip the coin? ";
    cin >> num;
    
    srand((time(0))); // seeding the random number generator
    
    for (int i = 1; i <= num; i++)
    {
        randomNumber = coinToss();
        if (randomNumber == 1)
            flipflop = "Heads";
        else
            flipflop = "Tails";
        
        cout << flipflop << endl;
        
    }
    return 0;
}


int coinToss(void)
{
    int randomNumber;
    randomNumber = 1 + rand() % 2;
    return randomNumber;
    
}
