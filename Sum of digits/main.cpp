/*
 * Class room example by the Instructor
 * Hone work assignment 6 (week 2) 
 */

/*
 * Name: Kent Hanson
 * Student ID: 1168787
 * Date:  September 23, 2017, 7:23 PM
 * HW:
 * Problem:
 * I certify this is my own work and code
 */

#include <iostream>        
#include <cstdlib>
#include <cctype>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    // homework assignment 6
    int numSize = 100;
    char input[numSize];
    
    cin >> input; // Get input from user
    
    int sum = 0; // total sum of each digit
    int loc = 0; //The location in the c-string
    int sum2 = 0;
    
    
    while (input[loc] != '\0')
    {
        // Solution 1
        string temp;
        temp += input[loc];
        
        // atoi converts from asci to int
        // cctype library
        sum += atoi(temp.c_str());
        
        // Solution 2
        char cTemp[2];
        
        // Move character into temporary array
        cTemp[0] = input[loc];
        cTemp[1] = '\0';
        sum2 += atoi(cTemp);
        
        loc++; // Increment the location
    }
    
    cout << "Sum is " << sum << endl;
    cout << "Sum2 is " << sum2 << endl;
    return 0;
}

