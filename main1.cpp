/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
* Name: Kent Hanson
* Student ID: 1168787
* Date: Sept. 16, 2017
* HW: Customer Accounts
* Problem:
* I certify this is my own work and code
*/


#include <iostream>        
#include <cstdlib>
#include <string>
#include <cstring>
#include <cctype>
#include <iomanip>

using namespace std;



const int FNAME = 30;
const int LNAME = 30;
const int M_ADDRESS = 45;
const int R_ADDRESS = 45;
const int CITY = 20;
const int STATE = 2;
const int ZIP = 5;
const int PHONE = 11;
const int ACCOUNT_BALANCE = 11;

// Structures
struct C_A // C_A = Customer Accounts
{
    char firstName[FNAME];            // Customer's first name
    char lastName[LNAME];             // Customer's last name
    char mailingAddress[M_ADDRESS];       // Customer's mailing address
    char residentialAddress[R_ADDRESS];   // Customer's residential address
    char city[CITY];                     // Customer's residential location   
    char state[STATE];                    // Customer's home state
    char  zipCode[ZIP];                // Customer's zip code   
    char   phoneNumber[PHONE];            // Customer's phone Number
    double accountBalance[ACCOUNT_BALANCE];         // Customer's Account Balance   
    int    lastPayment;            // Customer's Last Payment   
};

//Prototypes 
void creatingAccount(long);
void displayCustomerAccountInformation(long);


/*
 * 
 */
int main(int argc, char** argv) 
{
    //const int ACCOUNT_HOLDERS = 10; // Number of account holders
    int choice;
    do
    {
        
        // The main menu function display below here
        cout << "*********** Main Menu ****************\n\n";
        cout << "1. Create a new account.\n";
        cout << "2. Display a Customer's Account.\n";
        cout << "3. Delete a Customer's Account.\n";
        cout << "4. Change Customer's Account Information.\n";
        cout << "5. Quit the Program.\n";

        do
        {
          // Switch statements

          cout << "\nPlease enter a number in the range 1 - 5\n";
          cin >> choice;
        } while (choice < 1 || choice > 5);

        switch(choice)
          {
            case 1: 
                    creatingAccount(0);
                    break;
            case 2: cout << "You have selected item 2.";
                    break;
            case 3: cout << "You have selected item 3.";
                    break;
            case 4: cout << "You have selected item 4.";
                    break;
            case 5: cout << "You have selected item 5.";
                    break;
          }
    } while (choice != 5);
    return 0;
}

// Creating Customer Accounts information

void creatingAccount(long newAccount)
{
 /*
  * project log: ran into an error when the program got to a point to ask the user 
  * name of city, state, zip code. The program by pass them like they didn't exist
  */
    int CustomerValidInformation;
    C_A clientInfo;
    
    // const int ACCOUNT_HOLDERS = 10; // Number of account holders
    // int count;
    
    //for (int count = 0; count < ACCOUNT_HOLDERS; count++)
    do
    {    
        CustomerValidInformation = 1;
        cout << "Enter the Customer's first: " << endl;
        cin >> clientInfo.firstName, 30;
        cout << "Enter customer's last name: " << endl;
        cin >> clientInfo.lastName, 30;
        cout << "Enter customer's mailing address: " << endl;
        cin >> clientInfo.mailingAddress, 45;
        cout << "Enter customer's residential address: " << endl;
        // error here
        cin >> clientInfo.residentialAddress, 45;
        cout << "Enter customer's home city: " << endl;
        cin >> clientInfo.city, 20;
        cout << "Enter customer's state: " << endl;
        cin >> clientInfo.state, 2;
        cout << "Enter customer's zip code: " << endl;
        cin >> clientInfo.zipCode, 5;
        cout << "Enter customer's day time phone number: " << endl;
        cin >> clientInfo.phoneNumber, 11;
        cout << " Enter customer's account balance: " << endl;
        cin >> clientInfo.accountBalance, 11;
        cout << "Enter the date of last payment: " << endl;
        cin >> clientInfo.lastPayment;
        
        if (strlen(clientInfo.firstName) == 0 || strlen(clientInfo.lastName) == 0 || strlen(clientInfo.mailingAddress) == 0 || strlen(clientInfo.residentialAddress) == 0 || strlen(clientInfo.city) == 0 || strlen(clientInfo.state) == 0)
        {
            cout << "Enter valid information into each field. Thank you!\n";
            CustomerValidInformation = 0;
        } 
        else if (clientInfo.accountBalance < 0)
        {
            cout << "Please enter the account balance";
            CustomerValidInformation = 0;
        }
    } while (!CustomerValidInformation);
   
}

// Displaying Customer's Account Information

void displayCustomerAccountInformation(long)
{
    
}

